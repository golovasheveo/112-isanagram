package telran.text;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class FunctionalTestAppl {

    public static void main(String[] args) {
        String strings[] = {"p","lmn","abc","l","lmno" };
        //sorting using lambda expression sorting by string lengths
        Arrays.sort(strings, (a, b) -> a.length() - b.length());
        System.out.println(Arrays.toString(strings));
        //sorting using lambda closure sorting by string lengths and natural
        Arrays.sort(strings, (a, b) -> {
            int res = a.length() - b.length();
            return res == 0 ? a.compareTo(b) : res;
        });
        System.out.println(Arrays.toString(strings));
        //sorting using method reference; sorting by string lengths and natural
        Arrays.sort(strings, FunctionalTestAppl::compareLengthNatural);

        Map<Integer, String> studentMap = new HashMap<> ();
        studentMap.put(101, "Mahesh");
        studentMap.put(102, "Suresh");

        String newValue = studentMap.compute(101, (k, v) -> v + "-" + k);

        System.out.println(newValue);
        System.out.println(studentMap);



    }
    static  private Integer compareLengthNatural(String s1, String s2) {
        int res = Integer.compare(s1.length(), s2.length());
        return res == 0 ? s1.compareTo(s2) : res;
    }

}
