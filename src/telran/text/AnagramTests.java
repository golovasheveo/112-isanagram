//  !With merge
package telran.text;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static telran.text.Anagram.isAnagram;

class AnagramTests {
    String word = "yellow";

    @BeforeEach
    void setUp ( ) throws Exception {
    }

    @Test
    void testAnagramTrue ( ) {
        assertTrue ( isAnagram ( word, "lowley" ) );
        assertTrue ( isAnagram ( word, "woleyl" ) );

        assertTrue ( isAnagram ( word, "llowye" ) );
    }

    @Test
    void testAnagramFalse ( ) {
        assertFalse ( isAnagram ( word, "" ) );
        assertFalse ( isAnagram ( word, "lowlly" ) );

        assertFalse ( isAnagram ( word, "wolleyw" ) );
        assertFalse ( isAnagram ( word, "abcd" ) );
    }

}

