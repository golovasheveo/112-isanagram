//  !With merge
package telran.text;

import java.util.HashMap;

public class Anagram {
    public static boolean isAnagram ( String word, String anagram ) {

        if ( word.length ( ) != anagram.length ( ) ) return false;

        HashMap<Character, Integer> wordMap
                = getCharCounts ( word );

        for ( int i = 0; i < anagram.length ( ); i++ ) {

            int count = wordMap.getOrDefault ( anagram.charAt ( i ), 0 );
            if ( count != 0 ) {
                wordMap.put ( anagram.charAt ( i ), --count);
            } else return false;
        }

        return true;
    }

    private static HashMap<Character, Integer> getCharCounts ( String string ) {

        HashMap<Character, Integer> res
                = new HashMap<> ( );

        for ( int i = 0; i < string.length ( ); i++ ) {
            res.put ( string.charAt ( i ), res.getOrDefault ( string.charAt ( i ), 0 ) + 1 );
        }

        return res;

    }
}